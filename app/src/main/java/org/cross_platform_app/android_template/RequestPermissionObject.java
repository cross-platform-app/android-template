package org.cross_platform_app.android_template;

interface RequestPermissionObject {
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults);
}
