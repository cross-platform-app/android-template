package org.cross_platform_app.android_template;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;


public class GraphicsFragment extends Fragment {
    private FrameLayout frameLayoutGraphics;
    private GraphicsView graphicsView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_graphics, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        frameLayoutGraphics = getActivity().findViewById(R.id.frameLayoutGraphics);
        graphicsView = new GraphicsView(getActivity().getApplicationContext());
        frameLayoutGraphics.addView(graphicsView);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        frameLayoutGraphics = getActivity().findViewById(R.id.frameLayoutGraphics);
        if(hidden) {
            frameLayoutGraphics.removeView(graphicsView);
        } else {
            graphicsView = new GraphicsView(getActivity().getApplicationContext());
            frameLayoutGraphics.addView(graphicsView);
        }
    }

}
