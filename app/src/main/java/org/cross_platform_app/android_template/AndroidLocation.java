package org.cross_platform_app.android_template;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import org.cross_platform_app.core.LocationInterface;
import org.cross_platform_app.core.LocationRecord;
import org.cross_platform_app.core.OnChangeListener;

import java.util.List;
import java.util.Random;

/**
 * This class is the platform-specific implementation of LocationInterface from the Core-Library.
 * It is able to fetch location-data from the Android API and return a LocationRecord, that can then
 * be processed in the Core.
 */
public class AndroidLocation extends LocationInterface implements RequestPermissionObject {
    // reference to the MainActivtiy. We need this since requesting API permissions only is possible
    // from the MainActivity. *sigh*
    private MainActivity activity;

    // stores the request Code. Is needed to find out if the onRequestPermissionResult callback was
    // really meant for us. *doublesigh*
    private int requestCode;

    // onChangeListener that will be called back when updating the location was successful (or not).
    // Since this can only store one OnChangeListener, it is recommended to use an instance of this
    // class only once.
    OnChangeListener onChangeListener;
    private LocationRecord locationRecord = new LocationRecord(0,0);

    /**
     * Constructor.
     * @param activity pass the reference MainActivity here
     */
    public AndroidLocation(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void updateLocation(OnChangeListener onChangeListener) {

        this.onChangeListener = onChangeListener;
        // generate a unique request Code.
        requestCode = (int )(Math.random() * 50 + 1);

        // check for the permission to fetch the geolocation
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // if we do have permission, we can continue fetching
            fetchLocationFromAPI();
        } else {
            // if we don't have permission, we need to ask for it
            activity.onRequestPermissionsResultSubscribe(this);
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode);
        }
    }

    /**
     * This callback method gets called from MainActivity when the user has granted permission (or not)
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        activity.onRequestPermissionsResultUnsubscribe(this);
        // we check if this truly was our request for permission, by comparing the requestCode
        if(this.requestCode == requestCode) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fetchLocationFromAPI();
            } else {
                onChangeListener.onError();
            }
        }
    }

    /**
     * Fetching the current Location from the system. If we are not allowed to access the location,
     * onError is called on the onChangeListener. If the new location data is available, onSuccess
     * is called.
     */
    private void fetchLocationFromAPI() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_FINE);
                    LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
                    locationManager.requestSingleUpdate(criteria, new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            locationRecord = new LocationRecord(location.getLatitude(), location.getLongitude());
                            onChangeListener.onSuccess();
                        }

                        @Override
                        public void onStatusChanged(String s, int i, Bundle bundle) {}

                        @Override
                        public void onProviderEnabled(String s) {}

                        @Override
                        public void onProviderDisabled(String s) {}
                    }, null);
                } catch (SecurityException e) {
                    onChangeListener.onError();
                }
            }
        });


    }

    @Override
    public LocationRecord getLocation() {
        return locationRecord;
    }
}
