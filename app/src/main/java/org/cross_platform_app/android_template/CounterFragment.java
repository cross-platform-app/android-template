package org.cross_platform_app.android_template;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.cross_platform_app.core.CounterInterface;
import org.cross_platform_app.core.OnChangeListener;

/**
 * This fragment demonstrates a simple use-case on how to interact with the Logic-Tier implemented in
 * C++.
 *
 * This class has the purpose to demonstrate the following:
 * - How to interact with objects from the **Core**
 * - How to make use of the **OnChangeListener** interface.
 * - How to handle errors with the **OnChangeListener**
 */
public class CounterFragment extends Fragment {
    private TextView textViewCounter;
    private ProgressBar progressBarCounter;
    private FloatingActionButton floatingActionButtonIncreaseCounter;
    private Button buttonResetCounter;
    private CounterInterface counter;
    private Snackbar snackbarIncreaseCounterError;
    private FragmentActivity fragmentActivity;

    public CounterFragment() {
        counter = CounterInterface.Counter();
        counter.onNumberChanged(new OnChangeListener() {
            @Override
            public void onSuccess() {
                fragmentActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        textViewCounter.setText(Integer.toString(counter.getNumber()));
                        setStateCounterCanInteract(true);
                    }
                });

            }
            @Override
            public void onError() {
                fragmentActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        showCounterError();
                        setStateCounterCanInteract(true);
                    }
                });
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_counter, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fragmentActivity = getActivity();
        textViewCounter = getActivity().findViewById(R.id.textViewCounter);
        progressBarCounter = getActivity().findViewById(R.id.progressBarCounter);
        floatingActionButtonIncreaseCounter = getActivity().findViewById(R.id.floatingActionButtonIncreaseCounter);
        buttonResetCounter = getActivity().findViewById(R.id.buttonResetCounter);
        textViewCounter.setText(Integer.toString(counter.getNumber()));

        snackbarIncreaseCounterError = Snackbar.make(getView(), "Increasing the counter failed!", Snackbar.LENGTH_SHORT)
                .setAction("Reset", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onButtonResetCounterClicked();
                    }
                });

        floatingActionButtonIncreaseCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            onButtonIncreaseCounterClicked();
            }
        });

        buttonResetCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            onButtonResetCounterClicked();
            }
        });
    }

    /**
     * Function to be called when the "Increase Counter"-Button has been clicked.
     * Blocks the UI and calls the increase-function in the native Logic-Tier.
     */
    public void onButtonIncreaseCounterClicked()
    {
        setStateCounterCanInteract(false);
        // needs to be run in a new thread to keep the UI responsive when calculation in the
        // Logic-Tier takes a little longer.
        new Thread(new Runnable() {
            public void run() {
                counter.increaseNumber();
            }
        }).start();
    }

    /**
     * Function to be called when the "Reset"-Button has been clicked.
     * Blocks the UI and calls the reset-function in the native Logic-Tier.
     */
    public void onButtonResetCounterClicked()
    {
        setStateCounterCanInteract(false);
        new Thread(new Runnable() {
            public void run() {
                counter.resetNumber();
            }
        }).start();
    }

    /**
     * Sets the state on the UI.
     * @param canInteract The UI can have two different states:
     *                     - `true`: the user can interact with the elements. This means that he can
     *                               click the "Increase Counter"-Button and the "Reset"-Button.
     *                     - `false`: the user is not allowed to interact with the elements, because
     *                               the last asynchronous state-change has not yet called back to
     *                               the UI. This means that the buttons can not be clicked and a
     *                               rotating progress-indicator is showing up.
     */
    public void setStateCounterCanInteract(boolean canInteract)
    {
        if(canInteract) {
            buttonResetCounter.setClickable(true);
            floatingActionButtonIncreaseCounter.setClickable(true);
            progressBarCounter.setVisibility(ProgressBar.INVISIBLE);
        } else {
            buttonResetCounter.setClickable(false);
            floatingActionButtonIncreaseCounter.setClickable(false);
            progressBarCounter.setVisibility(ProgressBar.VISIBLE);
        }
    }

    /**
     * Displays a toast with an error-message if something went wrong while increasing the counter.
     */
    public void showCounterError()
    {
        snackbarIncreaseCounterError.show();
    }
}
