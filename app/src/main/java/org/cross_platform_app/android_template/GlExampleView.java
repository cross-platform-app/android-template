package org.cross_platform_app.android_template;

import android.content.Context;
import android.opengl.GLSurfaceView;

import org.cross_platform_app.glExample.GlExampleInterface;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Custom View that configures the OpenGL-Environment in which the "gl-example" OpenGL-Codebase should
 * be run in. This also defines and initializes the Renderer-class that manages the rendering.
 */
public class GlExampleView extends GLSurfaceView {

    public GlExampleView(Context context) {
        super(context);
        // Pick an EGLConfig with RGB8 color, 16-bit depth, no stencil,
        // supporting OpenGL ES 2.0 or later backwards-compatible versions.
        setEGLConfigChooser(8, 8, 8, 0, 16, 0);
        setEGLContextClientVersion(3);

        setRenderer(new OpenGLRenderer());
        // this makes the Renderer run in an infinite loop.
        setRenderMode(RENDERMODE_CONTINUOUSLY);
    }

    private static class OpenGLRenderer implements GLSurfaceView.Renderer {

        static {
            System.loadLibrary("glExample--JNI");
        }

        private GlExampleInterface glExample;

        public void onDrawFrame(GL10 gl) {
            init();
            glExample.render();

        }

        public void onSurfaceChanged(GL10 gl, int width, int height) {
            init();
            glExample.setViewport(width, height);

        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            init();
            glExample.setColor(.98f,.98f,.98f);
        }

        public void init() {
            if(glExample == null) {
                glExample = GlExampleInterface.GlExample();
            }
        }
    }
}
