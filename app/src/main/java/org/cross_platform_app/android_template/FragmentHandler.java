package org.cross_platform_app.android_template;


import android.support.v4.app.Fragment;

/**
 * This singleton-class is used to hold a reference on all fragments in this example-app, even if
 * the MainActivity gets destroyed due to screen-rotation or some other interrupt.
 *
 * <br>
 * <b>Disclaimer:</b> I'm pretty new to Android-development but I am aware that this is not a nice way of
 * solving my problem. The original Problem for me was that the references to the objects from the
 * native Code (from the "Core"), that I had instantiated in the Fragments, got lost because Android kills
 * it's Activities all the time.
 * A proper software-architecture would provide some singletons in the Logic-Tier to provide access to static
 * operations. But this wasn't needed in this example-implementations because on all platforms but
 * Android the references to the native Codebase in the UI tend to live long enough to provide a proof of
 * concept. Not so on Android... so I quickly came up with this workaround without putting to much
 * thought into it. So please CONSUME THIS WITH CAUTION!
 */
public class FragmentHandler {
    private static final FragmentHandler OBJ = new FragmentHandler();

    public CounterFragment counterFragment;
    public GeolocationFragment geolocationFragment;
    public OpenGLFragment openGLFragment;
    public GraphicsFragment graphicsFragment;
    public Fragment activeFragment;

    private FragmentHandler() {
        counterFragment = new CounterFragment();
        geolocationFragment = new GeolocationFragment();
        openGLFragment = new OpenGLFragment();
        graphicsFragment = new GraphicsFragment();

        activeFragment = counterFragment;
    }

    public static FragmentHandler getInstance() {
        return OBJ;
    }
}