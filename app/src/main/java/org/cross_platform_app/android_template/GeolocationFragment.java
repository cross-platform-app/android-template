package org.cross_platform_app.android_template;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.cross_platform_app.core.LocationRecord;
import org.cross_platform_app.core.LocationServiceInterface;
import org.cross_platform_app.core.OnChangeListener;

/**
 * Fragment that demonstrates how to consume a system-API like the geolocation through an
 * abstraction-Layer provided by the Logic-Tier.
 */
public class GeolocationFragment extends Fragment {

    private TextView textViewLatitude;
    private TextView textViewLongitude;
    private ProgressBar progressBarLoadingLocation;
    private FloatingActionButton floatingActionButtonUpdateLocation;
    private Snackbar snackbarUpdateLocationError;
    private FragmentActivity fragmentActivity;

    private LocationServiceInterface locationService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_geolocation, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        textViewLatitude = getActivity().findViewById(R.id.textViewLatitude);
        textViewLongitude = getActivity().findViewById(R.id.textViewLongitude);
        progressBarLoadingLocation = getActivity().findViewById(R.id.progressBarLoadingLocation);
        floatingActionButtonUpdateLocation = getActivity().findViewById(R.id.floatingActionButtonUpdateLocation);
        fragmentActivity = getActivity();

        snackbarUpdateLocationError = Snackbar.make(getView(), "Fetching Geolocation failed!", Snackbar.LENGTH_SHORT)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onFloatingActionButtonUpdateLocationClicked();
                    }
                });
        floatingActionButtonUpdateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFloatingActionButtonUpdateLocationClicked();
            }
        });

        // The Location-service should only be instantiated once. But it cannot be moved to the
        // constructor, like it was done in CounterFragment, since AndroidLocation can only be
        // initialized if the Activity is already created.
        // Please have a look at the comment in FragmentHandler. A better architecture would
        // eliminate such problems.
        if(locationService == null) {
            // Required empty public constructor
            locationService = LocationServiceInterface.LocationService(new AndroidLocation((MainActivity) getActivity()));

            locationService.onLocationChanged(new OnChangeListener() {
                @Override
                public void onSuccess() {
                    fragmentActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            updateLocationDisplay();
                            setStateDeviceLocationLoading(false);
                        }
                    });
                }

                @Override
                public void onError() {
                    fragmentActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            showLocationError();
                            setStateDeviceLocationLoading(false);
                        }
                    });
                }
            });
        }

        // load the last fetched location. This is necessary to set the data again after one of
        // Androids attempts to kill the Activity, e.g. after screen-rotation
        updateLocationDisplay();
    }

    /**
     * Should be called when the "UpdateLocation"-button has been clicked. Runs the location-update
     * in a separate Thread.
     */
    public void onFloatingActionButtonUpdateLocationClicked() {
        setStateDeviceLocationLoading(true);
        new Thread(new Runnable() {
            public void run() {
                locationService.updateLocation();
            }
        }).start();
    }

    /**
     * Shows a toast with an error message if updating the location failed for some reason.
     */
    public void showLocationError() {
        snackbarUpdateLocationError.show();
    }

    /**
     * Sets the state of the UI-elements related to fetching the device-location.
     * @param active The UI can have two states:
     *                - `true`:  The new location is currently being loaded. This means that the UI
     *                           waits for a system callback and the user can not interact with the
     *                           "Update Location"-Button. Additionally a rotating progress-indicator
     *                           is giving feedback to the user.
     *                - `false`: The user can request a new Location. This means that the button can
     *                           be clicked.
     */
    public void setStateDeviceLocationLoading(boolean active) {
        if(active) {
            progressBarLoadingLocation.setVisibility(ProgressBar.VISIBLE);
            floatingActionButtonUpdateLocation.setClickable(false);
        } else {
            progressBarLoadingLocation.setVisibility(ProgressBar.INVISIBLE);
            floatingActionButtonUpdateLocation.setClickable(true);
        }
    }

    /**
     * Method that updates the TextViews that display the current location.
     * Requests the new location-data on the `locationService` and renders the values as strings.
     */
    public void updateLocationDisplay()
    {
        LocationRecord location = locationService.getLocation();
        textViewLatitude.setText(Double.toString(location.getLat()));
        textViewLongitude.setText(Double.toString(location.getLon()));
    }

}
