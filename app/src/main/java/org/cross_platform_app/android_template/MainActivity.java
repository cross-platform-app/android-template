package org.cross_platform_app.android_template;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentContainer;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import java.util.ArrayList;

/**
 * Activity that is launched on startup. Holds together all the fragments in a "BottomNavigationView"
 */
public class MainActivity extends AppCompatActivity {
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("Core--JNI");
    }

    // pointer to the FragmentHandler-singleton, that holds the fragments
    private FragmentHandler fragmentHandler;

    // This list manages all current permission-requests on this activity. Is needed to forward
    // "onRequestPermissionResult"s to their owners. Have a look at the methods "onRequestPermissionsResultSubscribe",
    // "onRequestPermissionsResultUnsubscribe" and "onRequestPermissionsResult" to get a better
    // understanding on what is going on.
    private ArrayList<RequestPermissionObject> onRequestPermissionsResult = new ArrayList<RequestPermissionObject>();

    private BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentHandler = FragmentHandler.getInstance();

        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_counter:
                        loadFragment(fragmentHandler.counterFragment);
                        return true;
                    case R.id.navigation_opengl:
                        loadFragment(fragmentHandler.openGLFragment);
                        return true;
                    case R.id.navigation_geolocation:
                        loadFragment(fragmentHandler.geolocationFragment);
                        return true;
                    case R.id.navigation_graphics:
                        loadFragment(fragmentHandler.graphicsFragment);
                        return true;
                }
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        // add Fragments to FragmentContainer
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container, fragmentHandler.geolocationFragment).hide(fragmentHandler.geolocationFragment);
        transaction.add(R.id.fragment_container, fragmentHandler.openGLFragment).hide(fragmentHandler.openGLFragment);
        transaction.add(R.id.fragment_container, fragmentHandler.counterFragment).hide(fragmentHandler.counterFragment);
        transaction.add(R.id.fragment_container, fragmentHandler.graphicsFragment).hide(fragmentHandler.graphicsFragment);
        transaction.commit();

        // show counter Fragment on startup
        loadFragment(fragmentHandler.activeFragment);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // remove Fragments from FragmentContainer
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(fragmentHandler.geolocationFragment);
        transaction.remove(fragmentHandler.openGLFragment);
        transaction.remove(fragmentHandler.counterFragment);
        transaction.remove(fragmentHandler.graphicsFragment);
        transaction.commit();
    }

    /**
     * helper function to load a fragment in the foreground. The passed fragment needs to be already
     * added to the FragmentContainer. Hides the currently active fragment and shows the new one.
     * @param fragment fragment to be displayed
     */
    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(fragmentHandler.activeFragment != null) {
            transaction.hide(fragmentHandler.activeFragment);
        }
        fragmentHandler.activeFragment = fragment;
        transaction.show(fragment).commit();
    }

    /**
     * An API-wrapper like AndroidLocation can subscribe to requestPermission-Callbacks from the System.
     * Don't forget to unsubscribe if you don't expect callbacks any more.
     * <br>
     * <b>Disclaimer:</b> This architecture is problematic, but I've not come up with a better
     * solution yet. The major problem is the fact, that API-permissions can only be requested
     * from an activity, not from some other object. The "AndroidLocation"-wrapper tries to abstract
     * API-access for the Logic-Tier and therefore it makes sense that he also handles the permission
     * requesting. But to provide this functionality, a reference to this activity has to be passed
     * to "AndroidLocation" and all requestPermission-callbacks need to be forwarded.
     * This binds "AndroidLocation" to this Activity, it is totally impossible to use it from another
     * Activity, which is not acceptable.
     *
     * An alternative could be to request all required permissions on application-startup and
     * stop the application if the user doesn't allow them, which is also not a good idea.
     *
     * Another idea could be to create some kind of custom-class that implements the forwarding logic
     * and require that all activities in the application must inherit from it. This would for sure
     * lead to other problems.
     * @param object class that implements the "RequestPermissionObject"-interface.
     */
    public void onRequestPermissionsResultSubscribe(RequestPermissionObject object) {
        onRequestPermissionsResult.add(object);
    }

    /**
     * unsubscribe from requestPermisson-callbacks
     * @param object
     */
    public void onRequestPermissionsResultUnsubscribe(RequestPermissionObject object) {
        onRequestPermissionsResult.remove(object);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // this distributes the system-callback to all subscribers. The subscribers need to find out
        // if the callback was meant for them by comparing the requestCode
        for(int i = 0; i < onRequestPermissionsResult.size(); i++) {
            onRequestPermissionsResult.get(i).onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
