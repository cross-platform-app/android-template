package org.cross_platform_app.android_template;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Fragment that demonstrates the usage of OpenGL ES in Android.
 */
public class OpenGLFragment extends Fragment {
    private FrameLayout frameLayoutOpenGL;
    private GlExampleView glExampleView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_open_gl, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        frameLayoutOpenGL = getActivity().findViewById(R.id.frameLayoutOpenGL);
        glExampleView = new GlExampleView(getActivity().getApplicationContext());
        frameLayoutOpenGL.addView(glExampleView);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        frameLayoutOpenGL = getActivity().findViewById(R.id.frameLayoutOpenGL);
        if(hidden) {
            frameLayoutOpenGL.removeView(glExampleView);
        } else {
            glExampleView = new GlExampleView(getActivity().getApplicationContext());
            frameLayoutOpenGL.addView(glExampleView);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // the rendering-loop needs to be paused on application-pause
        glExampleView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        glExampleView.onResume();
    }
}
