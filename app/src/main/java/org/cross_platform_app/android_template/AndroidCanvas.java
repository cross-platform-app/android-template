package org.cross_platform_app.android_template;

import android.graphics.Path;

import org.cross_platform_app.graphics.CanvasInterface;
import org.cross_platform_app.graphics.Paint;
import org.cross_platform_app.graphics.Skalar;

import java.util.ArrayList;

public class AndroidCanvas extends CanvasInterface {

    private android.graphics.Canvas canvas;

    AndroidCanvas(android.graphics.Canvas canvas) {
        this.canvas = canvas;
    }

    @Override
    public void save() {
        canvas.save();
    }

    @Override
    public void restore() {
        canvas.restore();
    }

    @Override
    public void translate(Skalar skalar) {
        canvas.translate(skalar.getX(), skalar.getY());
    }

    @Override
    public void drawLines(ArrayList<Skalar> points, Paint paint) {
        android.graphics.Paint strokePaint = new android.graphics.Paint();
        strokePaint.setARGB(
                Math.round(paint.getStrokeColor().getAlpha() * 255) ,
                Math.round(paint.getStrokeColor().getRed() *255),
                Math.round(paint.getStrokeColor().getGreen() *255),
                Math.round(paint.getStrokeColor().getBlue() *255)
        );

        strokePaint.setStyle(android.graphics.Paint.Style.STROKE);
        strokePaint.setStrokeWidth(paint.getLineWidth());
        android.graphics.Paint fillPaint = new android.graphics.Paint();
        fillPaint.setARGB(
                Math.round(paint.getFillColor().getAlpha() * 255) ,
                Math.round(paint.getFillColor().getRed() *255),
                Math.round(paint.getFillColor().getGreen() *255),
                Math.round(paint.getFillColor().getBlue() *255)
        );
        fillPaint.setStyle(android.graphics.Paint.Style.FILL);

        Path path = new Path();
        path.moveTo(points.get(0).getX(), points.get(0).getY());

        for(int i = 1; i < points.size(); i++) {
            path.lineTo(points.get(i).getX(), points.get(i).getY());
        }

        switch(paint.getStyle()) {
            case FILL:
                canvas.drawPath(path, fillPaint);
                break;
            case STROKE:
                canvas.drawPath(path, strokePaint);
                break;
            case FILLANDSTROKE:
                canvas.drawPath(path, fillPaint);
                canvas.drawPath(path, strokePaint);
                break;
        }

    }
}
