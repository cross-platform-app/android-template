package org.cross_platform_app.android_template;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import org.cross_platform_app.graphics.GraphicsInterface;

public class GraphicsView extends View {

    static {
        System.loadLibrary("Graphics--JNI");
    }



    private AndroidCanvas androidCanvas = null;
    private GraphicsInterface graphics = null;

    public GraphicsView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public GraphicsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }
    public GraphicsView(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(androidCanvas == null && graphics == null) {
            androidCanvas = new AndroidCanvas(canvas);
            graphics = GraphicsInterface.Graphics(androidCanvas);
        }
        graphics.draw();
    }
}
