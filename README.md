# Cross-platform-app: Android Client

This template/demo shows how to use a C++ based business-logic in an Android application.

The template therefore provides 3 different demos:

1. The basic usage of the [C++-library](https://gitlab.com/cross-platform-app/core-template).
2. The rendering of a [C++-based OpenGL (ES) implementation](https://gitlab.com/cross-platform-app/gl-example).
3. An example how to access a System-API, like the device-location via the [C++-library](https://gitlab.com/cross-platform-app/core-template).

## Build Dependencies
- cmake >= 3.7.1 (because of the usage of Hunter)

**Important:** Make sure to also meet the dependencies in the submodules `Core` and `gl-example` in `app/src/main/cpp/`.

## Build Instructions
1. Initialize all submodules with `git submodule update --init --recursive`

2. get a Android Studio Version that supports cmake 3.7.1 or higher. (This
    is not as trivial as it sounds, since currently Android Studio is just
    compatible with a custom fork of CMake version 3.4; **Android Studio 3.2**
    will support newer CMake versions.
    
3. Tell Gradle about your local CMake installation by adding the installation
    path to `local.properties` by adding a line like this:
    ```
    cmake.dir=/usr/local
    ```
    
4. Sync the Project. If you have Issues with CMake Configuration, there
    seems to be no possible way to inspect the CMake-Log in **Android Studio 3.2**;
    I recommend running gradle in the commandline with `./gradlew build --info`
    since there you can read the CMake-Log.
    
5. Build the Project and have fun!

## Documentation

The Javadoc-documentation can be generated like this:
```
./gradlew generateDebugJavadoc
./gradlew generateReleaseJavadoc
```

or to run javadoc task for all variants:
```
./gradlew generateJavadoc
```

The output can be found in the folder `app/javaDoc`

_Javadoc-generation uses [this gradle plugin](https://github.com/vanniktech/gradle-android-javadoc-plugin)._

## Screenshots
- application running on Android:

![Screenshots of the Demo](screenshot.png)

- the same application running on ChromeOS (as you can see the geolocation didn't work in the emulator):

![Screenshots of the Demo on ChromeOS](screenshot_chromeos.png)
## Troubleshooting
### Gradle Sync
If you get the error `Gradle sync failed: Error occurred while communicating with CMake server.` during project sync, have fun trying this bugfix I documented in build.gradle:
```
    [build.gradle]
    ...
    dependencies {
        // if the error
        // > Gradle sync failed: Error occurred while communicating with CMake server.
        // pops up, change classpath to
        // 'com.android.tools.build:gradle:3.2.0-alpha07'
        // then resync gradle task. This is expected to fail with the error
        // > Gradle sync failed: Cause: com.intellij.openapi.externalSystem.model.ExternalSystemException:
        // > ...
        // Now switch back to 3.0.1 and resync. The build should work now.
        // This worked for me, no idea why, I wish you good luck!
        classpath 'com.android.tools.build:gradle:3.2.0-alpha07'

    ...
```
This sounds kind of weird and it really is, I can't explain but I found it to be solving my problems.